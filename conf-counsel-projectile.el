;; counsel-projectile
(use-package counsel-projectile
  :after projectile
  :config (counsel-projectile-mode))
