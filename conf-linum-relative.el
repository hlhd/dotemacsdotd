;; linum-relative
(use-package linum-relative
  :init
  (setq linum-relative-backend 'display-line-numbers-mode)
  (linum-relative-global-mode))
