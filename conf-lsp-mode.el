;; lsp-mode
(use-package lsp-mode
  :commands (lsp lsp-deferred)
  :init
  (setq lsp-keymap-prefix "C-c l")
  :config (lsp-enable-which-key-integration t))

;; lsp-ui
(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode))

;; lsp-treemacs
(use-package lsp-treemacs
  :after lsp)
