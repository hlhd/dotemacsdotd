;; multiple cursors
(use-package multiple-cursors
  :init
  (setq mc/cmds-to-run-for-all nil)
  (setq mc/cmds-to-run-once '(swiper-mc)))
