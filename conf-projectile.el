;; projectile
(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :init
  (when (file-directory-p "~/PycharmProjects")
    (setq projectile-project-search-path '("~/PycharmProjects")))
  (when (file-directory-p "~/mysql")
    (setq projectile-project-search-path '("~/mysql")))
  (when (file-directory-p "~/dots")
    (setq projectile-project-search-path '("~/dots")))
  (setq projectile-switch-project-action #'projectile-dired))
