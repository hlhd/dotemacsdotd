;; info
;; modularize emacs config
;; https://stackoverflow.com/questions/2079095/how-to-modularize-an-emacs-configuration#2079146

(defconst user-init-dir
  (cond ((boundp 'user-emacs-directory)
         user-emacs-directory)
        ((boundp 'user-init-directory)
         user-init-directory)
        (t "~/.emacs.d/")))

(defun load-user-file (file)
  (interactive "f")
  "Load a file in current user's configuration directory"
  (load-file (expand-file-name file user-init-dir)))

;; load user file
(load-user-file "main.el")
(load-user-file "main-package.el")
(load-user-file "unconfig-packages.el")
(load-user-file "conf-ivy-ivy-rich.el")
(load-user-file "conf-doom-modeline.el")
(load-user-file "conf-doom-themes.el")
(load-user-file "conf-rainbow-delimiters.el")
(load-user-file "conf-which-key.el")
(load-user-file "conf-helpful.el")
(load-user-file "conf-general.el")
(load-user-file "conf-hydra.el")
(load-user-file "conf-projectile.el")
(load-user-file "conf-counsel-projectile.el")
(load-user-file "conf-magit.el")
(load-user-file "conf-org-bullets.el")
(load-user-file "conf-visual-fill-column.el")
(load-user-file "conf-org.el")
(load-user-file "conf-modus-themes.el")
(load-user-file "conf-linum-relative.el")
(load-user-file "conf-multiple-cursor.el")
(load-user-file "conf-lsp-mode.el")
(load-user-file "conf-python-mode.el")
(load-user-file "conf-pyvenv.el")
(load-user-file "conf-typescript-mode.el")
(load-user-file "conf-company-mode.el")
(load-user-file "conf-haskell-mode.el")
(load-user-file "conf-flycheck.el")
(load-user-file "conf-ox-hugo.el")
(load-user-file "conf-org-roam.el")

;; to keep emacs custom stop appear in here
(setq custom-file (concat user-emacs-directory "by-custom.el"))
(load-user-file "by-custom.el")
