;; https://www.emacswiki.org/emacs/EmacsLispMode

;; disable emacs welcome screen
(setq inhibit-startup-message t)

;; disable visible scrollbar
(scroll-bar-mode -1)

;; disable toolbar
(tool-bar-mode -1)

;; give some breathing room
(set-fringe-mode 10)

;; enable visual bell
(setq visible-bell t)

;; change emacs global font
(set-face-attribute 'default nil :font "RobotoMono Nerd Font" :height 116)

;; line number
(column-number-mode)

;; autopair
(electric-pair-mode)

;; make ESC quit escape
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

;; disable line numbers for some mode
(dolist (mode '(org-mode-hook
                org-agenda-mode-hook
                term-mode-hook
                eshell-mode-hook
                magit-mode-hook
                ivy-mode-hook
                xref--xref-buffer-mode-hook
                treemacs-mode-hook
                vterm-mode-hook))
  (add-hook mode (lambda () (linum-relative-mode 0))))

;; emacs-lisp-mode hook
(add-hook 'emacs-lisp-mode-hook
          (lambda ()
            ;; Use space not tabs.
            (setq indent-tabs-mode nil)
            ;; Pretty print eval'd expressions.
            (define-key emacs-lisp-mode-map
              "\C-x\C-e" 'pp-eval-last-sexp)))
